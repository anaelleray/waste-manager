<?php

namespace App\Service;
use App\Wastes\Waste;

class Compost{

     public $toBurn = [];
     public $typeOf = [];
     public $maxWeight = int;

     public function __construct(array $toBurn, string $typeOf){
          
          $this->$toBurn = null;
          $this->$typeOf = ['paper','organic','glass', 'metal', 'plastic', 'other'];
          $this->$maxWeight = 100;
     }

     public function getSpecWaste(OrganicWaste $organicType, PaperWaste $paperType, GlassWaste $glassType, MetalWaste $metalType, PlasticWaste $plasticType, OtherWaste $otherType, array $toBurn, array $typeOf) {

          //Property type ?? ($$.type.value ?)
          if($this->$organicType($type) === $typeOf && $this->$paperType($type) === $typeOf && $this->$glassType($type) === $typeOf && $this->$metalType($type) === $typeOf && $this->$plasticType($type) === $typeOf && $this->$pvcType($type) === $typeOf && $this->$petType($type) === $typeOf && $this->$pcType($type) === $typeOf && $this->$pehdType($type) === $typeOf && $this->$otherType($type) === $typeOf){

               array_push($this->$toBurn, $paperType, $organicType, $glassType, $metalType, $plasticType, $otherType);
          }
          return $toBurn;
     }
}