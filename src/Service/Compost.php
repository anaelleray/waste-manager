<?php

namespace App\Service;
use App\Wastes\Waste;

class Compost{

     public $toCompost = [];
     public $typeOf = [];
     public $maxWeight = int;

     public function __construct(array $toCompost, string $typeOf){
          
          $this->$toCompost = null;
          $this->$typeOf = ['organic'];
          $this-> $maxWeight = 100;

     }

     public function getSpecWaste(OrganicWaste $organicType, array $toCompost, array $typeOf) {

          //Property type ?? ($$.type.value ?)
          if($this->$organicType($type) === $typeOf){
               array_push($this->$toCompost, $organicType);
          }
          return $toCompost;
     }
}