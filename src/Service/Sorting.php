<?php

namespace App\Service;

use App\Wastes\Waste;

class Sorting{

     public function sortingWaste(array $array, string $type) : array {
          switch ($type) {
          case 'paper':
               array_push($array, new PaperWaste($type, $paperType[$type]));
               return $array;
          break;
          case 'organic':
               array_push($array, new OrganicWaste($type, $organicType[$type]));
               return $array;
          break;
          case 'glass':
               array_push($array, new GlassWaste($type, $glassType[$type]));
               return $array;
          break;
          case 'metal':
               array_push($array, new MetalWaste($type, $metalType[$type]));
               return $array;
          break;
          case 'plastic' :
               array_push($array, new PlasticWaste($type, $plasticType[$type]));
               return $array;            
          break;
          case 'other':
               array_push($array, new OtherWaste($type, $otherType[$type]));
               return $array;
          break;
          
          }
     }
}