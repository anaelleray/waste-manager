<?php

namespace App\Service;
use App\Wastes\Waste;

class Compost{

     public $toRecycle = [];
     public $typeOf = [];
     public $maxWeight = int;

     public function __construct(array $toRecycle, string $typeOf){
          
          $this->$toRecycle = null;
          $this->$typeOf = ['paper','glass', 'metal', 'plastic'];
          $this->$maxWeight = 100;
     }

     public function getSpecWaste(PaperWaste $paperType, GlassWaste $glassType, MetalWaste $metalType, PlasticWaste $plasticType, array $toRecycle, array $typeOf) {

          //Property type ?? ($$.type.value ?)
          if($this->$paperType($type) === $typeOf && $this->$glassType($type) === $typeOf && $this->$metalType($type) === $typeOf && $this->$plasticType($type) === $typeOf && $this->$pvcType($type) === $typeOf && $this->$petType($type) === $typeOf && $this->$pcType($type) === $typeOf && $this->$pehdType($type)){

               array_push($this->$toRecycle, $paperType, $glassType, $metalType, $plasticType);
          }
          return $toRecycle;
     }
}