<?php 

namespace App;

use App\Wastes\Waste;


class JsonDecode {

     public function __construct(string $file){

          $jsondata = file_get_contents($file);
          $jsonDecode = json_decode($jsondata, true);

     }
     public function wasteZone()
     {
          $wasteZoneArray = json_decode($this->file, true);
          return $wasteZoneArray["quartiers"];
     }


     public function wasteSorting()
     {
          $wasteSortingArray = json_decode($this->file, true);
          return $wasteSortingArray["services"];
     }
}

