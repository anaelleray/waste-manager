<?php

require __DIR__ . ('../vendor/autoload.php');

use App\Wastes\Waste;

$wasteZone = new JsonDecode(file_get_contents('data.json'));
$sortingService = new Sorting();

$type = ['paper', 'plastic', 'organic', 'metal', 'glass', 'other'];

$paperWasteArray = [];
$organicWasteArray = [];
$metalWasteArray = [];
$glassWasteArray = [];
$otherWasteArray = [];

$incinerator = [];

foreach ($wasteZone->wasteZone() as $waste) {

     $paperWasteArray = $sortingService->sortWaste($paperWasteArray, $waste, $type[0]);
     $organicWasteArray = $sortingService->sortWaste($organicWasteArray, $waste, $type[2]);
     $metalWasteArray = $sortingService->sortWaste($metalWasteArray, $waste, $type[3]);
     $glassWasteArray = $sortingService->sortWaste($glassWasteArray, $waste, $type[4]);
     $otherWasteArray = $sortingService->sortWaste($otherWasteArray, $waste, $type[5]);

}

foreach ($wasteZone->wasteSorting() as $wasteSorting) {
     //if(){}
}

/** @var 
 * App\Entity\Waste\PaperWaste $paperWaste 
 * */
foreach ($paperWasteArray as $paperWaste) {
     $paperTreatment->treatment($paperWaste);
}

/** 
 * @var App\Entity\Waste\GlassWaste $glassWaste
 *  */
foreach ($glassWasteArray as $glassWaste) {
     $glassTreatment->treatment($glassWaste);
}

/** @var App\Entity\Waste\MetalWaste 
 * $metalWaste 
 * */
foreach ($metalWasteArray as $metalWaste) {
     $metalTreatment->treatment($metalWaste);
}
